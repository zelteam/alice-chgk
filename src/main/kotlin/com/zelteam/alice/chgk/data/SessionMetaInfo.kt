package com.zelteam.alice.chgk.data

import com.zelteam.alice.chgk.config.QUESTIONS_TO_WIN

data class SessionMetaInfo(var currentQuestion: Question?,
                           var aliceScore: Int = 0,
                           var userScore: Int = 0) {

    fun isNew() = aliceScore == userScore && aliceScore == 0
    fun isCrucial() = aliceScore == QUESTIONS_TO_WIN - 1 || userScore == QUESTIONS_TO_WIN - 1
    fun isFinished() = aliceScore == QUESTIONS_TO_WIN || userScore == QUESTIONS_TO_WIN
    fun isAliceWin() = aliceScore == QUESTIONS_TO_WIN
}