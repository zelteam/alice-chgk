package com.zelteam.alice.chgk.data

data class Question(val text: String, val answer: String)