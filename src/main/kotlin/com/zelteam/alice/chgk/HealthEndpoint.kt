package com.zelteam.alice.chgk


import com.zelteam.alice.chgk.service.AliceService
import com.zelteam.alice.tools.data.AliceInput
import com.zelteam.alice.tools.data.Meta
import com.zelteam.alice.tools.data.Request
import com.zelteam.alice.tools.data.SessionInput
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class HealthEndpoint(val aliceService: AliceService) {
    @GetMapping("/defaultSession/{phrase}")
    fun ping(@PathVariable phrase: String) = aliceService.request(AliceInput(
            meta = Meta("","",""),
            session = SessionInput(false, 1, "defaultSession", "", ""),
            request = Request(phrase, "", phrase),
            version = "1.0"
    ))
}