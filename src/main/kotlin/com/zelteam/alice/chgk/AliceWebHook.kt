package com.zelteam.alice.chgk

import com.zelteam.alice.chgk.service.AliceService
import com.zelteam.alice.tools.data.AliceInput
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class AliceWebHook(val aliceService: AliceService) {
    @PostMapping("/alice-chgk")
    fun aliceChgk(@RequestBody aliceInput: AliceInput) = aliceService.request(aliceInput)
}