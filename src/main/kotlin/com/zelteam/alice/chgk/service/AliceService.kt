package com.zelteam.alice.chgk.service

import com.zelteam.alice.chgk.config.*
import com.zelteam.alice.chgk.data.Question
import com.zelteam.alice.chgk.data.SessionMetaInfo
import com.zelteam.alice.chgk.service.chgk.ChgkBaseQuestionsService
import com.zelteam.alice.tools.data.AliceInput
import com.zelteam.alice.tools.data.AliceOutput
import com.zelteam.alice.tools.data.SessionInput
import com.zelteam.alice.tools.service.buildAliceOutput
import com.zelteam.alice.tools.service.containsWord
import org.springframework.stereotype.Service

@Service
class AliceService(val chgkBaseQuestionsService: ChgkBaseQuestionsService) {
    private val sessions: MutableMap<String, SessionMetaInfo?> = hashMapOf()

    fun request(aliceInput: AliceInput): AliceOutput {
        val command = aliceInput.request.command.trim().toLowerCase()
        return when {
            aliceInput.session.new -> buildAliceOutput(WELCOME, aliceInput)
            sessions[aliceInput.session.sessionId]?.currentQuestion != null -> answer(aliceInput, command)
            ifNextQuestion(command) -> question(aliceInput)
            ifFinish(command) -> endSession(aliceInput)
            else -> buildAliceOutput(UNKNOWN.plus(NEXT_QUESTION), aliceInput)
        }
    }

    private fun endSession(aliceInput: AliceInput): AliceOutput {
        return buildAliceOutput(GOOD_BYE, aliceInput, endSession = true)
    }

    private fun answer(aliceInput: AliceInput, answer: String): AliceOutput {
        val sessionMetaInfo = sessions[aliceInput.session.sessionId]
        val currentQuestion = sessionMetaInfo?.currentQuestion!!
        val isAnswerRight = chgkBaseQuestionsService.checkAnswer(currentQuestion, answer)
        when {
            isAnswerRight -> sessionMetaInfo.userScore++
            else -> sessionMetaInfo.aliceScore++
        }
        val resultText = (if (isAnswerRight) RIGHT else WRONG).plus(currentQuestion.answer).plus(".")
                .plus(when {
                            sessionMetaInfo.isFinished() && sessionMetaInfo.isAliceWin() -> LOSE
                            sessionMetaInfo.isFinished() -> WIN
                            else -> ""
                        })
                .plus(SCORE).plus(sessionMetaInfo.userScore).plus(":").plus(sessionMetaInfo.aliceScore).plus("\n")
                .plus(if (sessionMetaInfo.isFinished()) NEXT_GAME else NEXT_QUESTION)

        putQuestionToSession(aliceInput.session, null, newGame = sessionMetaInfo.isFinished())

        return buildAliceOutput(resultText, aliceInput)
    }

    private fun question(aliceInput: AliceInput): AliceOutput {
        val newQuestion = chgkBaseQuestionsService.nextQuestion()
        val currentSessionMetaInfo = putQuestionToSession(aliceInput.session, newQuestion)
        val introQuestion = when {
            currentSessionMetaInfo.isNew() -> FIRST_QUESTION_INTRO
            currentSessionMetaInfo.isCrucial() -> CRUCIAL_QUESTION_INTRO
            else -> QUESTION_INTRO
        }
        return buildAliceOutput(introQuestion.plus(newQuestion.text), aliceInput)
    }

    private fun putQuestionToSession(sessionInput: SessionInput, question: Question?, newGame: Boolean = false): SessionMetaInfo {
        val sessionMetaInfo = sessions[sessionInput.sessionId]
        when {
            sessionMetaInfo != null && !newGame -> sessionMetaInfo.currentQuestion = question
            else -> sessions[sessionInput.sessionId] = SessionMetaInfo(question)
        }
        return sessions[sessionInput.sessionId]!!
    }


    private fun ifNextQuestion(phrase: String) = containsWord(phrase,
            "вопрос|еще|ещё|да|следующий|начнём|продолжим|продолж")

    private fun ifFinish(phrase: String) = containsWord(phrase,
            "нет|закончить|хватит|всё")

}

