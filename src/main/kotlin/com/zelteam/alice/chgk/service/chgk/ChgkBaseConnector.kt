package com.zelteam.alice.chgk.service.chgk

import com.zelteam.alice.chgk.data.Question
import org.jsoup.Jsoup
import org.springframework.stereotype.Service

@Service
class ChgkBaseConnector {
    fun findNewQuestions(size: Int): List<Question> {
        Jsoup.connect("https://baza-voprosov.ru/random/from_2010-01-01/answers/types1/complexity1/limit".plus(size))
                .validateTLSCertificates(false)
                .get().run {
                    return select("div.random_question").map { questionDiv ->
                        val answer = questionDiv.select(":root > p")
                                .first { it.select(":root > strong").first()?.text()?.contains("Ответ") ?: false }
                                .ownText()
                        Question(questionDiv.ownText(), answer)
                    }.toList()
                }
    }
}