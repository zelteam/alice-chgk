package com.zelteam.alice.chgk.service.chgk

import com.zelteam.alice.chgk.config.EASTER_QUESTION
import com.zelteam.alice.chgk.config.MIN_QUESTIONS_QUEUE_SIZE
import com.zelteam.alice.chgk.config.QUESTIONS_REQUEST_SIZE
import com.zelteam.alice.chgk.config.info
import com.zelteam.alice.chgk.data.Question
import kotlinx.coroutines.experimental.launch
import org.springframework.stereotype.Service
import java.util.*
import javax.annotation.PostConstruct

@Service
class ChgkBaseQuestionsService(val chgkBaseConnector: ChgkBaseConnector) {
    val questions: Queue<Question> = LinkedList()

    @PostConstruct
    private fun init() = feedQuestions()

    fun nextQuestion(isEaster: Boolean = false): Question {
        if (isEaster) return EASTER_QUESTION
        checkQueueSize()
        return questions.poll()!!
    }

    fun checkAnswer(question: Question, answer: String): Boolean {
        val userAnswer = moderateAnswer(answer)
        val isCorrect = when {
            question.answer.length <= 4 || answer.length <= 4 -> question.answer == userAnswer
            else -> question.answer == userAnswer
                    || question.answer.contains(userAnswer)
                    || userAnswer.contains(question.answer)
        }
        info("Checking answer. Result: $isCorrect. User answer: `$userAnswer`. Question answer: `${question.answer}`")
        return isCorrect
    }

    private fun checkQueueSize() {
        when {
            questions.isEmpty() -> feedQuestions()
            questions.size < MIN_QUESTIONS_QUEUE_SIZE -> launch { feedQuestions() }
        }
    }

    private fun feedQuestions() {
        questions.addAll(moderateQuestions(chgkBaseConnector.findNewQuestions(QUESTIONS_REQUEST_SIZE)))
    }

    private fun moderateAnswer(answer: String): String =
            answer.replace(".", " ").replace("  ", " ").toLowerCase().trim()

    private fun moderateQuestions(newQuestions: List<Question>): Collection<Question> =
            newQuestions
                    .filter { !(it.text.contains('[') && it.text.contains(']')) } // no comments and replacements in question
                    .filter { !(it.answer.contains('[') && it.answer.contains(']')) } // no comments and replacements in answer
                    .filter { it.text[0].isLetter() } // no images in question
                    .filter { it.answer[0].isLetter() } // no images in answer
                    .filter { it.text.length < 1024 } // not huge question
                    .filter { it.answer.length < 1024 } // not huge answer
                    .map {
                        it.copy(text = it.text.trim(), answer = it.answer
                                .replace(".", " ").replace("  ", " ").toLowerCase().trim())
                    }
                    .toList()

}
