package com.zelteam.alice.chgk.config

const val NEXT_QUESTION = " {Ещё вопросик?/Готовы к следующему вопросу?/У меня есть ещё вопросы, продолжим?}"
const val NEXT_GAME = " {Сыграем ещё?/Сыграем ещё раз?/Ещё игру?}"
const val WELCOME = "Добро пожаловать в игру ЧГК! Я задаю вопросы, Вы на них отвечаете. Начнём?"
const val RIGHT = "{Правильно/Верно/Идеально/Абсолютно точно}, "
const val WRONG = "{Не верно/Мимо/Не совсем так/Опять ошиблись}. {Правильный ответ/А правильно/Ответ}: "
const val GOOD_BYE = "Спасибо за игру! {Пока/До встречи}!"
const val QUESTION_INTRO = "{Внимание, вопрос/Вопрос/Следующий вопрос}:\n"
const val CRUCIAL_QUESTION_INTRO = "Решающий вопрос:\n"
const val FIRST_QUESTION_INTRO = "Итак, счёт 0:0, играем до $QUESTIONS_TO_WIN очков. Первый вопрос:\n"
const val UNKNOWN = "{Я Вас не очень поняла./Не понятно...}"
const val SCORE = " Счёт Игрок - Алиса: "
const val WIN = " {С Победой!/Поздравляю с победой!/Вы победили!}"
const val LOSE = " {Я выиграла!/В этот раз победа за мной!/Я победила, в следующий раз вам повезёт!}"

