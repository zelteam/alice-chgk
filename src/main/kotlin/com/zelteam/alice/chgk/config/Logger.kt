package com.zelteam.alice.chgk.config

import com.zelteam.alice.chgk.config.LogHelper.LOG
import org.slf4j.LoggerFactory

object LogHelper {
    val LOG = LoggerFactory.getLogger(LogHelper.javaClass)!!
}

fun info(message: String) {
    LOG.info(message)
}
