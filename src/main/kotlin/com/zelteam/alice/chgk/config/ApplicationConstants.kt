package com.zelteam.alice.chgk.config

import com.zelteam.alice.chgk.data.Question

const val MIN_QUESTIONS_QUEUE_SIZE = 20
const val QUESTIONS_REQUEST_SIZE = 100
const val QUESTIONS_TO_WIN = 3
val EASTER_QUESTION =
        Question(text = "По данным Британской ассоциации разработчиков Программного обеспечения за 2018 год, лучшим программистом в мире является... Кто?",
                answer = "Эдуард Гринченко")