package com.zelteam.alice.chgk

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication



@SpringBootApplication
class AliceChgkApplication



fun main(args: Array<String>) {
    runApplication<AliceChgkApplication>(*args)
}
